import random
import numpy as np
from cs231n.data_utils import load_CIFAR10
import matplotlib.pyplot as plt

from cs231n.features import color_histogram_hsv, hog_feature


def get_CIFAR10_data(num_training=49000, num_validation=1000, num_test=1000):
    # Load the raw CIFAR-10 data
    cifar10_dir = 'cs231n/datasets/cifar-10-batches-py'
    X_train, y_train, X_test, y_test = load_CIFAR10(cifar10_dir)

    # Subsample the data
    mask = list(range(num_training, num_training + num_validation))
    X_val = X_train[mask]
    y_val = y_train[mask]
    mask = list(range(num_training))
    X_train = X_train[mask]
    y_train = y_train[mask]
    mask = list(range(num_test))
    X_test = X_test[mask]
    y_test = y_test[mask]

    return X_train, y_train, X_val, y_val, X_test, y_test

X_train, y_train, X_val, y_val, X_test, y_test = get_CIFAR10_data()

from cs231n.features import *

num_color_bins = 10  # Number of bins in the color histogram
feature_fns = [hog_feature, lambda img: color_histogram_hsv(
    img, nbin=num_color_bins)]
X_train_feats = extract_features(X_train, feature_fns, verbose=True)
X_val_feats = extract_features(X_val, feature_fns)
X_test_feats = extract_features(X_test, feature_fns)

# Preprocessing: Subtract the mean feature
mean_feat = np.mean(X_train_feats, axis=0, keepdims=True)
X_train_feats -= mean_feat
X_val_feats -= mean_feat
X_test_feats -= mean_feat

# Preprocessing: Divide by standard deviation. This ensures that each feature
# has roughly the same scale.
std_feat = np.std(X_train_feats, axis=0, keepdims=True)
X_train_feats /= std_feat
X_val_feats /= std_feat
X_test_feats /= std_feat

# Preprocessing: Add a bias dimension
X_train_feats = np.hstack(
    [X_train_feats, np.ones((X_train_feats.shape[0], 1))])
X_val_feats = np.hstack([X_val_feats, np.ones((X_val_feats.shape[0], 1))])
X_test_feats = np.hstack([X_test_feats, np.ones((X_test_feats.shape[0], 1))])

# Use the validation set to tune the learning rate and regularization strength
'''
from cs231n.classifiers.linear_classifier import LinearSVM

learning_rates = [1e-7, 2.5e-6]
regularization_strengths = [1e3, 4e5]

results = {}
best_val = -1
best_svm = None

##########################################################################
# TODO:                                                                        #
# Use the validation set to set the learning rate and regularization strength. #
# This should be identical to the validation that you did for the SVM; save    #
# the best trained classifer in best_svm. You might also want to play          #
# with different numbers of bins in the color histogram. If you are careful    #
# you should be able to get accuracy of near 0.44 on the validation set.       #
##########################################################################
for rate in np.linspace(learning_rates[0], learning_rates[1], 15):
    for reg in np.linspace(regularization_strengths[0], regularization_strengths[1], 15):
        svm = LinearSVM()
        svm.train(X_train_feats, y_train, learning_rate=rate,
                  reg=reg, num_iters=500)
        train_accuracy = np.mean(svm.predict(X_train_feats) == y_train)
        val_accuracy = np.mean(svm.predict(X_val_feats) == y_val)
        results[(rate, reg)] = (train_accuracy, val_accuracy)
        if (val_accuracy > best_val):
            best_val = val_accuracy
            best_svm = svm
##########################################################################
#                              END OF YOUR CODE                                #
##########################################################################

# Print out results.
for lr, reg in sorted(results):
    train_accuracy, val_accuracy = results[(lr, reg)]
    print('lr %e reg %e train accuracy: %f val accuracy: %f' % (
        lr, reg, train_accuracy, val_accuracy))
'''
'''
print('best validation accuracy achieved during cross-validation: %f' % best_val)

import math
x_scatter = [math.log10(x[0]) for x in results]
y_scatter = [math.log10(x[1]) for x in results]

# plot training accuracy
marker_size = 100
colors = [results[x][0] for x in results]
plt.subplot(2, 1, 1)
plt.scatter(x_scatter, y_scatter, marker_size, c=colors)
plt.colorbar()
plt.xlabel('log learning rate')
plt.ylabel('log regularization strength')
plt.title('CIFAR-10 training accuracy')

# plot validation accuracy
colors = [results[x][1] for x in results]  # default size of markers is 20
plt.subplot(2, 1, 2)
plt.scatter(x_scatter, y_scatter, marker_size, c=colors)
plt.colorbar()
plt.xlabel('log learning rate')
plt.ylabel('log regularization strength')
plt.title('CIFAR-10 validation accuracy')
plt.show()


# Evaluate your trained SVM on the test set
y_test_pred = best_svm.predict(X_test_feats)
test_accuracy = np.mean(y_test == y_test_pred)
print(test_accuracy)

# An important way to gain intuition about how an algorithm works is to
# visualize the mistakes that it makes. In this visualization, we show examples
# of images that are misclassified by our current system. The first column
# shows images that our system labeled as "plane" but whose true label is
# something other than "plane".

examples_per_class = 8
classes = ['plane', 'car', 'bird', 'cat', 'deer',
           'dog', 'frog', 'horse', 'ship', 'truck']
for cls, cls_name in enumerate(classes):
    idxs = np.where((y_test != cls) & (y_test_pred == cls))[0]
    idxs = np.random.choice(idxs, examples_per_class, replace=False)
    for i, idx in enumerate(idxs):
        plt.subplot(examples_per_class, len(
            classes), i * len(classes) + cls + 1)
        plt.imshow(X_test[idx].astype('uint8'))
        plt.axis('off')
        if i == 0:
            plt.title(cls_name)
plt.show()
'''

from cs231n.classifiers.neural_net import TwoLayerNet

input_dim = X_train_feats.shape[1]
hidden_dim = 500
num_classes = 10

net = TwoLayerNet(input_dim, hidden_dim, num_classes)
best_net = None

##########################################################################
# TODO: Train a two-layer neural network on image features. You may want to    #
# cross-validate various parameters as in previous sections. Store your best   #
# model in the best_net variable.                                              #
##########################################################################
input_size = X_train_feats.shape[1]
hidden_size = 50
num_classes = 10
learning_rate_range = [1e-4, 5e-4]
reg_range = [0.15, 0.4]
batch_size_range = [300, 500]
hidden_layer_range = [20, 100]
best_net = None
best_params = (0, 0)
results = {}
best_val = -1
n = 0
stats = net.train(X_train_feats, y_train, X_val_feats, y_val,
                  num_iters=15000, batch_size=200,
                  learning_rate=1e-3, learning_rate_decay=0.95,
                  reg=5e-6, verbose=True)
train_accuracy = stats['train_acc_history'][-1]
val_accuracy = stats['val_acc_history'][-1]
print(train_accuracy, val_accuracy)
'''
for rate in np.linspace(learning_rate_range[0], learning_rate_range[1], 1):
    for reg in np.linspace(reg_range[0], reg_range[1], 1):
        n += 1
        print(n)
        net = TwoLayerNet(input_size, hidden_size, num_classes)
        stats = net.train(X_train_feats, y_train, X_val_feats, y_val,
                          num_iters=1000, batch_size=400,
                          learning_rate=rate, learning_rate_decay=0.95,
                          reg=reg, verbose=True)
        train_accuracy = stats['train_acc_history'][-1]
        val_accuracy = stats['val_acc_history'][-1]
        loss = stats['loss_history'][-1]
        results[(rate, reg)] = (train_accuracy, val_accuracy, loss)
        if (val_accuracy > best_val):
            best_val = val_accuracy
            best_net = net
            best_params = (rate, reg)
##########################################################################
#                              END OF YOUR CODE                                #
##########################################################################
# Run your neural net classifier on the test set. You should be able to
# get more than 55% accuracy.

test_acc = (net.predict(X_test_feats) == y_test).mean()
print(test_acc)
'''
