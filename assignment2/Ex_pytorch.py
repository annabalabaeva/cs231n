#!/usr/bin/env python3

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.utils.data import sampler

import torchvision.datasets as dset
import torchvision.transforms as T
import numpy as np

import time


class ChunkSampler(sampler.Sampler):
    """Samples elements sequentially from some offset.
    Arguments:
        num_samples: # of desired datapoints
        start: offset where we should start selecting from
    """

    def __init__(self, num_samples, start=0):
        self.num_samples = num_samples
        self.start = start

    def __iter__(self):
        return iter(range(self.start, self.start + self.num_samples))

    def __len__(self):
        return self.num_samples


def reset(m):
    if hasattr(m, 'reset_parameters'):
        m.reset_parameters()


class Flatten(nn.Module):

    def forward(self, x):
        N, C, H, W = x.size()  # read in N, C, H, W
        # "flatten" the C * H * W values into a single vector per image
        return x.view(N, -1)


def train(model, loss_fn, optimizer,learning_rate, scheduler, num_epochs=1, win_loss=[], win_acc=[]):
    for epoch in range(num_epochs):
        print('Starting epoch %d / %d' % (epoch + 1, num_epochs))
        #scheduler.step()
        if epoch in [4, 6, 9, 13]:
            learning_rate = 0.1 * learning_rate
            optimizer = optim.Adam(fixed_model.parameters(),
                           lr=learning_rate, weight_decay=3e-3)
        model.train()
        val_accuracy = 0
        train_accuracy = 0
        val_batch_iter = 0
        val_batch_list = list(enumerate(loader_val))
        val_batch_list_len = len(val_batch_list)
        for t, (x_tr, y_tr) in enumerate(loader_train):
            x_tr_var = Variable(x_tr.type(dtype))
            y_tr_var = Variable(y_tr.type(dtype).long())

            scores = model(x_tr_var)
            loss = loss_fn(scores, y_tr_var)
            # Print current loss
            if (t + 1) % print_every == 0:
                print('t = %d, loss = %.4f' % (t + 1, loss.data[0]))
            # Show current loss on plot
            if (t + 1) % show_loss_every == 0:
                if win_loss is not None:
                    # Get iteration number
                    X = np.array([t + epoch * len(loader_train)])

                    # Get and plot train loss
                    Y_tr_loss = np.array([loss.data[0]])
                    viz.line(X=X, Y=Y_tr_loss, win=win_loss[
                             0], name='train', update='append')
                    _, (x_val, y_val) = val_batch_list[
                        val_batch_iter % (val_batch_list_len - 1)]

                    # Get and plot validation loss
                    x_val_var = Variable(x_val.type(dtype))
                    y_val_var = Variable(y_val.type(dtype).long())
                    scores_val = model(x_val_var)
                    Y_val_loss = np.array(
                        [loss_fn(scores_val, y_val_var).data[0]])
                    viz.line(X=X, Y=Y_val_loss, win=win_loss[
                             0], name='validation', update='append')

                    # Increment current validation batch number
                    val_batch_iter += 1

            # Show current accuracy on plot
            if (t + 1) % check_acc_every == 0:
                if win_acc is not None:
                    # Get iteration number
                    X = np.array([t + epoch * len(loader_train)])

                    # Get validation and train accuracy
                    val_accuracy = check_accuracy(model, loader_val)
                    train_accuracy = check_accuracy(model, loader_train)
                    Y_val = np.array([val_accuracy])
                    Y_train = np.array([train_accuracy])

                    # Plot accuracies
                    viz.line(X=X, Y=Y_val, win=win_acc[
                             0], name='validation', update='append')
                    viz.line(X=X, Y=Y_train, win=win_acc[
                             0], name='train', update='append')

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # Print accuracies every epoch
        if train_accuracy == 0:
            check_accuracy(model, loader_train, print_acc=True)
        else:
            print("Train accuracy: ", train_accuracy)
        if val_accuracy == 0:
            check_accuracy(model, loader_val, print_acc=True)
        else:
            print("Validation accuracy: ", val_accuracy)


def check_accuracy(model, loader, print_acc=False):
    if print_acc:
        if loader.dataset.train:
            print('Checking accuracy on validation set')
        else:
            print('Checking accuracy on test set')
    num_correct = 0
    num_samples = 0
    model.eval()  # Put the model in test mode (the opposite of model.train(), essentially)
    for x, y in loader:
        x_var = Variable(x.type(dtype), volatile=True)

        scores = model(x_var)
        _, preds = scores.data.cpu().max(1)
        num_correct += (preds == y).sum()
        num_samples += preds.size(0)
    acc = float(num_correct) / num_samples
    if print_acc:
        print('Got %d / %d correct (%.2f)' %
              (num_correct, num_samples, 100 * acc))
    return(acc)


def parse_args():
    import argparse
    parser = argparse.ArgumentParser(description='Assig. #2')
    parser.add_argument('--host', type=str, default='http://localhost',
                        help='Visdom host (localhost by default)')
    parser.add_argument('--port', type=int, default=8097,
                        help='Visdom port (8097 by default)')
    parser.add_argument('--ui', action='store_true', default=False,
                        help='Enable Visdom UI')
    parser.add_argument('--check', type=int, help='Enable Visdom UI')
    args = parser.parse_args()
    return args


#####################################################################

if torch.cuda.is_available():
    dtype = torch.cuda.FloatTensor  # the CPU datatype
    print("GPU")
else:
    dtype = torch.FloatTensor
    print("CPU")


args = parse_args()
win_loss = None
win_acc = None

# Set window params if used
if args.ui:
    import visdom as viz
    viz = viz.Visdom(server=args.host, port=args.port)
    win_acc = []
    win_loss = []
    win_loss.append(viz.line(X=np.array([0]), Y=np.array([[0, 0]]), opts=dict(
        xlabel='Iteration',
        ylabel='Loss',
        title='Loss plot',
        legend=['train', 'validation'],
        showlegend=True
    )))
    win_acc.append(viz.line(X=np.array([0]), Y=np.array([[0, 0]]),
                            opts=dict(
        xlabel='Iteration',
        ylabel='Accuracy',
        title='Accuracy plot',
        legend=['train', 'validation'],
        showlegend=True
    )))
start_time = time.time()

# Set train and test data size, set show and print frequency
NUM_TRAIN = 49000
NUM_VAL = 1000
if args.check is not None:
    NUM_TRAIN = args.check
    print_every = int(NUM_TRAIN / 200)
    show_loss_every = int(NUM_TRAIN / 200)
    check_acc_every = int(NUM_TRAIN / 500)
else:
    NUM_TRAIN = 49000
    print_every = 100
    show_loss_every = 100
    check_acc_every = 300


# Load data
cifar10_train = dset.CIFAR10('./cs231n/datasets', train=True, download=True,
                             transform=T.ToTensor())
loader_train = DataLoader(cifar10_train, batch_size=64,
                          sampler=ChunkSampler(NUM_TRAIN, 0))
cifar10_val = dset.CIFAR10('./cs231n/datasets', train=True, download=True,
                           transform=T.ToTensor())
loader_val = DataLoader(cifar10_val, batch_size=64,
                        sampler=ChunkSampler(NUM_VAL, NUM_TRAIN))

cifar10_test = dset.CIFAR10('./cs231n/datasets', train=False, download=True,
                            transform=T.ToTensor())
loader_test = DataLoader(cifar10_test, batch_size=64)

'''
nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1),
    nn.ReLU(),
    nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
    nn.ReLU(),
    nn.MaxPool2d(kernel_size=2, stride=2),
'''
# Here's where we define the architecture of the model...
fixed_model_base = nn.Sequential(
    nn.BatchNorm2d(3),
    nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1),
    nn.ReLU(),
    nn.Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
    nn.ReLU(),
    nn.MaxPool2d(kernel_size=2, stride=2),

    nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1),
    nn.ReLU(),
    nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
    nn.ReLU(),
    nn.MaxPool2d(kernel_size=2, stride=2),
    Flatten(),
    nn.Linear(32 * 64, 512),
    nn.ReLU(inplace=True),
    nn.Dropout(p=0.2),
    nn.Linear(512, 128),
    nn.ReLU(inplace=True),
    nn.Dropout(p=0.1),
    nn.Linear(128, 10),  # affine layer
)

# Create model
fixed_model = fixed_model_base.type(dtype)
loss_fn = nn.CrossEntropyLoss().type(dtype)

# Set learning rate
learning_rate = 3e-3

# Check if we shuld us regularization
if args.check is not None:
    optimizer = optim.Adam(fixed_model.parameters(), lr=learning_rate)
else:
    optimizer = optim.Adam(fixed_model.parameters(),
                           lr=learning_rate, weight_decay=1e-3)

# Set learning rate decay
#scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[5, 9, 11, 13], gamma=0.1)

scheduler = None
# This sets the model in "training" mode. This is relevant for some layers that may have different behavior
# in training mode vs testing mode, such as Dropout and BatchNorm.
fixed_model.train()
torch.manual_seed(12345)
fixed_model.apply(reset)
train(fixed_model, loss_fn, optimizer, learning_rate, scheduler, num_epochs=15,
      win_loss=win_loss, win_acc=win_acc)
# Check final accuracy on validation
check_accuracy(fixed_model, loader_val)

# Check final accuracy on validation
check_accuracy(fixed_model, loader_test, print_acc=True)

print("--- %s seconds ---" % (time.time() - start_time))
